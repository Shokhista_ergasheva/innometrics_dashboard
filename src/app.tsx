import './app.css';
import React from 'react';
import { BrowserRouter } from "react-router-dom";
import 'date-fns';
import { Provider } from 'react-redux';
import {store} from './__data__/store';


// import Main from './containers/main/main';
import Dashboard from './containers/dashboard';
import NavBar from './containers/navbar/navbar';
import LayoutDrawer from './components/drawer/drawer';

const App = () => (
  <Provider store={store}>
    <BrowserRouter>
      <div className="root">
        <LayoutDrawer />
        <NavBar />
        <Dashboard />
      </div>
    </BrowserRouter>
  </Provider>
);

export default App;
