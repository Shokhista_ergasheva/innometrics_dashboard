import React from 'react';
import Main from './main/main';
import { URLs } from '../__data__/urls';
import { Switch, Route } from 'react-router-dom';
import Forms from './forms/forms';
import Login from './login/login';


const Dashboard = () => (


    <Switch>
        <Route exact path={URLs.root.url}>
            <Main/>
        </Route>
        <Route path={URLs.forms.url}>
            <Forms/>
        </Route>
        <Route path={URLs.login.url}>
            <Login/>
        </Route>
    </Switch>
                
);

export default Dashboard;