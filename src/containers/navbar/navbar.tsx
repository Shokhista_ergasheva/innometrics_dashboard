import React from 'react';
import MenuIcon from "@material-ui/icons/Menu";
import { makeStyles } from "@material-ui/core/styles";
import { Link as RouterLink } from 'react-router-dom';
import { AppBar, Button, IconButton, LinkProps, Toolbar, Typography } from '@material-ui/core';
import { URLs } from '../../__data__/urls';
import { useSelector } from 'react-redux';

// import styles from './style.css';

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  appbar: {
    position: 'relative',
    zIndex: 1300,
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  title: {
    flexGrow: 1,
  },
}));




export default function NavBar() {
  const data = useSelector((state: any) => state.login.data);
  const classes = useStyles();
  return (
    <AppBar className={classes.appbar}>
      <Toolbar>
        <IconButton
          edge="start"
          className={classes.menuButton}
          color="inherit"
          aria-label="menu"
        >
          <MenuIcon />
        </IconButton>
        <Typography variant="h6" className={classes.title}>
          Innometrics
          </Typography>
        {!data ? (
          <Button
            id = "btn-login"
            color="inherit"
            component={
              RouterLink
            }
            to={URLs.login.url}
          >
            Login
          </Button>
        ) : data.email}
      </Toolbar>
    </AppBar>
  );
}

