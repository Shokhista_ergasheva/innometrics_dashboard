import React, { useState} from "react";
import {Redirect} from "react-router-dom";
import style from "./style.css";
import {submitLogin} from '../../__data__/actions/login';
import {useDispatch, useSelector} from 'react-redux';
import {URLs} from '../../__data__/urls';



export default function Login() {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');

  const loading = useSelector((state:any) => state.login.loading)
  const error = useSelector((state:any) => state.login.error);
  const data = useSelector((state:any) => state.login.data);


  const dispatch = useDispatch();
 
  const handleSubmit = (event) => {
    event.preventDefault()
    console.log(email, password)
    dispatch(submitLogin(email, password));
  };

  if(data){
    return <Redirect to={URLs.root.url}/>
  }
  // const items=[{label:'Main', path:URLs.root.url}, {label:'Charts', path:URLs.forms.url}, {label:'Login', path:URLs.login.url}];
  return (
    <div className={style.container}>
      <h1 className={style.title}>Get started with</h1>
      <h1 className={style.companyName}>innometrics</h1>
      <div className={style.informationBox}>
        <form onSubmit={handleSubmit}>
          {error ? <h5>Couldnt login</h5> : null}
          {data && <h5>Successfully loged in</h5>}
          <label htmlFor="email">Email Address</label>
          <input type="text" id="email" value={email} disabled={loading} onChange={e => setEmail(e.target.value)} />
          <label htmlFor="password">Password</label>
          <input type="password" id="password" value={password} disabled={loading}  onChange={e => setPassword(e.target.value)} />
          <button className="loginButton" disabled={loading}  type="submit">Login</button>
        </form>
      </div>
    </div>
  );
}

