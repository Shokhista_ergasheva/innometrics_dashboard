import React from 'react';
import { mount } from 'enzyme';
import Forms from '../forms/forms';

import { describe, it, expect } from '@jest/globals'


describe("<Forms/>", () => {
    it('There is no error', ()=> {
        const wrapper = mount(<Forms/>);

        expect(wrapper.find('div')).toMatchSnapshot();
    });
});