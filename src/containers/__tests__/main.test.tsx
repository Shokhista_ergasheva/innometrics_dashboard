import React from 'react';
import { mount } from 'enzyme';
import Main from '../main/main';

import { describe, it, expect } from '@jest/globals'


describe("<Main/>", () => {
    it('There is no error', ()=> {
        const wrapper = mount(<Main/>);

        expect(wrapper.find('div')).toMatchSnapshot();
    });
});