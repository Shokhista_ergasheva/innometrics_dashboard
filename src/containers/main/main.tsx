import React from "react";
import ActivitiesChart from "../../components/activitiesChart/activitiesChart";
import CategoriesChart from "../../components/categoriesChart";
import styles from './style.css';


export default function Main() {
  return (
    <div className={styles.container}>
      <CategoriesChart />
      <ActivitiesChart/>
    </div>
  );
}
