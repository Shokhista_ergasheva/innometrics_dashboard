import { useMemo } from 'react';
import _ from 'lodash';

type ValidatorObject<T> = {
    check(value: T): boolean;
    getMessage(value?: T): string;
}

export const twoSameValue = (itemName): ValidatorObject<[unknown, unknown]> => ({
    check([value1, value2]) {
        return value1 === value2
    },
    getMessage() {
        return `Ваши ${itemName} не совпадают`
    }
});

export const treeSameChar = (itemName): ValidatorObject<string> => ({
    check(value) {
        return !/(.)\1{2}/.test(value);
    },
    getMessage(): string {
        return `В ${itemName} нельзя использовать 3 и более одинаковых символов`
    }
})

export const requireSpesialSymbols = (): ValidatorObject<string> => ({
    check(value) {
        return /[!?\-&_]/.test(value);
    },
    getMessage() {
        return `Ваш пароль должен содержать хотя бы один из симолов - !, ?, -, &, _`
    }
})

export const minMaxValidator = (min: number, max = Infinity): ValidatorObject<string> => ({
    check(value) {
        return value.length > min && value.length < max
    },
    getMessage() {
        if (max === Infinity) {
            return `Число символов должно быть больше чем ${min}`
        }

        return `Число символов должно быть больше чем ${min} и меньше чем ${max}`
    }
});

export const notEmptyValidator = (): ValidatorObject<Array<string | number>> => ({
    check(values) {
        return _(values)
            .map(v => !v || _.isNil(v))
            .value()
            .indexOf(true) === -1;
    },
    getMessage() {
        return 'Необходимо заполнить все обязательные поля';
    }
});

export const emailValidator = (): ValidatorObject<string> => ({
    check(value): boolean {
        return /^[\w.]{2,64}@[a-z]{2,16}\.[a-z]{2,3}$/i.test(value)
    },
    getMessage(): string {
        return `Ваш email не валиден`
    }
});

type ValidationResult = {
    isValid: boolean;
    message: string;
}

const validate = <T>(value: T , validatorsArray: Array<ValidatorObject<T>>): ValidationResult[] =>
    _.map(validatorsArray, v => ({
        isValid: v.check(value),
        message: v.getMessage(value)
    }))

export const useValidation = <T>(value: T, validatorsArray: Array<ValidatorObject<T>>) => {
    return useMemo(() => {
        return validate(value, validatorsArray)
    }, [value])
}

export const checkIsValid = (validatorsResults: ValidationResult[]) =>
    validatorsResults.findIndex(v => !v.isValid) === -1;
export const getFirstError = (validatorsResults: ValidationResult[]) =>
    validatorsResults.find(v => !v.isValid)?.message

type useFormValidation = <T extends { [key: string]: Array<ValidationResult> }>(obj: T) => {
    isFormValid: boolean,
    errors: { [key: string]: string }
}

export const useFormValidation: useFormValidation = obj => ({
    isFormValid: _.flatten(Object.values(obj)).findIndex(v => !v.isValid) === -1,
    errors: Object
        .entries(obj)
        .map(([fieldName, validators]) => [fieldName, getFirstError(validators)])
        .reduce((acc, [fieldName, error]) => ({
            ...acc,
            [fieldName]: error
        }), {})
})
