import React from 'react';
import { mount } from 'enzyme';
import LayoutDrawer from '../drawer/drawer';

import { describe, it, expect } from '@jest/globals'


describe("<LayoutDrawer/>", () => {
    it('There is no error', ()=> {
        const wrapper = mount(<LayoutDrawer/>);

        expect(wrapper.find('div')).toMatchSnapshot();
    });
});