import React from 'react';
import { mount } from 'enzyme';
import TextField from '../activitiesChart';
import { describe, it, expect } from '@jest/globals';

describe('<TextField/>', () => {
    it('Functioning without any errors', () => {


        const wrapper = mount(
            <TextField />
        );
        expect(wrapper).toMatchSnapshot();
    });
});

// describe('defines activitiesChart form details', () => {
//     it('renders user email field', () => {
//         const wrapper = mount(
//             <TextField />
//         );
//         const userEmailField = wrapper.find('input[name="userEmail"]')

//         expect(userEmailField.prop('label')).toBe('text');
//     });
// });