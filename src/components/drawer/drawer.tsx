import {
  Drawer,
  List,
  Toolbar,
  ListItem,
  ListItemText,
  ListItemIcon,
  Link,
} from "@material-ui/core";
import InboxIcon from "@material-ui/icons/MoveToInbox";
import React from "react";
import { URLs } from "../../__data__/urls";
import styles from './style.css';

export default function LayoutDrawer() {
  const items=[{label:'Main', path:URLs.root.url}, {label:'Forms', path:URLs.forms.url}];
  return (
    <Drawer className={styles.drawer}
      variant="permanent"
    >
      <Toolbar />
        <List>
          {items.map((item, index) => (
            <Link href={item.path} key={index}>
              <ListItem button>
              <ListItemIcon>
                <InboxIcon />
              </ListItemIcon>
              <ListItemText primary={item.label} />
              </ListItem>
            </Link>
          ))}
        </List>
    </Drawer>
  );
}
