import React from 'react';

import ErrorBoundary from '../error-boundary';
import style from './style.css';

type PageProps = {
    info: string;
}

const Page: React.FC<PageProps> = ({ children, info }) => (
    <ErrorBoundary>
        <div className={style.wrapper}>
            <div className={style.formPart}>
                <div className={style.paper}>
                    {children}
                </div>
            </div>
            <div className={style.infoBanner}>
                <span>{info}</span>
            </div>
        </div>
    </ErrorBoundary>
);

export default Page;
