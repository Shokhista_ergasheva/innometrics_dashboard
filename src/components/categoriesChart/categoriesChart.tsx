import React, { useCallback, useEffect, useState } from 'react';
import { ResponsiveBar } from '@nivo/bar'
import { authBhAxios } from '../../utils';
import Paper from '@material-ui/core/Paper';
import TextField from '@material-ui/core/TextField';
import {
    KeyboardDatePicker,
    MuiPickersUtilsProvider,
} from '@material-ui/pickers';
import DateFnsUtils from '@date-io/date-fns';
import qs from 'qs';
import _ from 'lodash';

import styles from './style.css';


const CategoriesChart = () => {
    const [data, setData] = useState([]);
    const [email, setEmail] = useState('');
    const [minDate, setMinDate] = useState(new Date(2021, 1, 26));
    const [maxDate, setMaxDate] = useState(new Date(2021, 1, 26));
    const query = {
        email,
        min_Date: minDate,
        max_Date: maxDate,
    };
    const addLeadingZeros = (value) => ('0' + value).slice(-2);
    const stripTime = (value) => {
        const [month, date, year] = value.toLocaleDateString("en-US")
            .split("/")
            .map((value, index) => {

                if (index != 2) {
                    return addLeadingZeros(value);
                } else {
                    return value.toString();
                }
            });
        return `${date}/${month}/${year}`;
    };
    const loadData = useCallback(() => {
        authBhAxios(`/categoryTimeReport?${qs.stringify(_(query).toPairs().filter((prop) => !!prop[1]).fromPairs().value(),
            { serializeDate: d => stripTime(d) }
        )}`, {
            method: 'get'
        })
            .then(value => { setData(value.data.report.map(item => ({ ...item, timeused: parseInt(item.timeused.split(':')[0], 10) }))) })
    }, [email, minDate, maxDate]);
    useEffect(() => { loadData() }, [loadData])
    return (
        <Paper className={styles.wrapper}>
            <div className={styles.form}>
                <TextField name="userEmail" label="email" value={email} onChange={(event) => setEmail(event.currentTarget.value)} ></TextField>
                <MuiPickersUtilsProvider utils={DateFnsUtils}>
                    <KeyboardDatePicker
                        className={styles.formInput}
                        disableToolbar
                        variant="inline"
                        format="dd/MM/yyyy"
                        label="Min date"
                        value={minDate}
                        onChange={val => setMinDate(val)}
                    />
                    <KeyboardDatePicker
                        className={styles.formInput}
                        disableToolbar
                        variant="inline"
                        format="dd/MM/yyyy"
                        label="Max date"
                        value={maxDate}
                        onChange={val => setMaxDate(val)}
                    />
                </MuiPickersUtilsProvider>
            </div>
            <div className={styles.chart}>
                <ResponsiveBar
                    data={data}
                    keys={['timeused']}
                    indexBy="catname"
                    margin={{ top: 50, right: 130, bottom: 50, left: 100 }}
                    padding={0.15}
                    layout="horizontal"
                    valueScale={{ type: 'linear' }}
                    indexScale={{ type: 'band', round: true }}
                    colors={{ scheme: 'pastel2' }}
                    defs={[
                        {
                            id: 'dots',
                            type: 'patternDots',
                            background: 'inherit',
                            color: '#38bcb2',
                            size: 4,
                            padding: 1,
                            stagger: true
                        },
                        {
                            id: 'lines',
                            type: 'patternLines',
                            background: 'inherit',
                            color: '#eed312',
                            rotation: -45,
                            lineWidth: 6,
                            spacing: 10
                        }
                    ]}
                    fill={[
                        {
                            match: {
                                id: 'fries'
                            },
                            id: 'dots'
                        },
                        {
                            match: {
                                id: 'sandwich'
                            },
                            id: 'lines'
                        }
                    ]}
                    borderColor={{ from: 'color', modifiers: [['darker', 1.6]] }}
                    axisTop={null}
                    axisRight={null}
                    axisBottom={null}
                    axisLeft={{
                        tickSize: 5,
                        tickPadding: 5,
                        tickRotation: 0,
                        // legend: 'Categories',
                        legendPosition: 'middle',
                        legendOffset: -40
                    }}
                    labelSkipWidth={12}
                    labelSkipHeight={12}
                    labelTextColor={{ from: 'color', modifiers: [['darker', 1.6]] }}
                    legends={[
                        {
                            dataFrom: 'keys',
                            anchor: 'bottom-right',
                            direction: 'column',
                            justify: false,
                            translateX: 120,
                            translateY: 0,
                            itemsSpacing: 2,
                            itemWidth: 100,
                            itemHeight: 20,
                            itemDirection: 'left-to-right',
                            itemOpacity: 0.85,
                            symbolSize: 20,
                            effects: [
                                {
                                    on: 'hover',
                                    style: {
                                        itemOpacity: 1
                                    }
                                }
                            ]
                        }
                    ]}
                    animate={true}
                    motionStiffness={90}
                    motionDamping={15}
                />
            </div>
        </Paper>
    )
}
export default CategoriesChart;