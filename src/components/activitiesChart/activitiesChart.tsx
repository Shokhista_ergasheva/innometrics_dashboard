import { ResponsiveLine } from '@nivo/line';
// import { ResponsivePie } from '@nivo/pie'
import Paper from '@material-ui/core/Paper';
import React, { useCallback, useState, useEffect } from 'react';
import { authBhAxios } from '../../utils';
import TextField from '@material-ui/core/TextField';
import {
    KeyboardDatePicker,
    MuiPickersUtilsProvider,
} from '@material-ui/pickers';
import DateFnsUtils from '@date-io/date-fns';
import qs from 'qs';
import _ from 'lodash';
import styles from './style.css';

const ActivitiesChart = () => {
    const [data, setData] = useState([]);
    const [email, setEmail] = useState('');
    const [minDate, setMinDate] = useState(new Date(2021, 1, 26));
    const [maxDate, setMaxDate] = useState(new Date(2021, 1, 26));
    const query = {
        email,
        min_Date: minDate,
        max_Date: maxDate,
    };
    const addLeadingZeros = (value) => ('0' + value).slice(-2);
    const stripTime = (value) => {
        const [month, date, year] = value.toLocaleDateString("en-US")
            .split("/")
            .map((value, index) => {

                if (index != 2) {
                    return addLeadingZeros(value);
                } else {
                    return value.toString();
                }
            });
        return `${date}/${month}/${year}`;
    };

    const loadData = useCallback(() => {
        // authBhAxios({
        //     method: 'GET',
        //     url: '/Reports/cumulativeReport',
        //     params: query
        // })
        authBhAxios(`/Reports/cumulativeReport?${qs.stringify(_(query).toPairs().filter((prop) => !!prop[1]).fromPairs().value(),
            { serializeDate: d => stripTime(d) }
        )}`, {
            method: 'get'
        })
            .then(value => { setData(value.data.report.map(item => ({ ...item, daily_sum: parseInt(item.daily_sum.split(':')[0], 10) }))) })
    }, [email, minDate, maxDate]);
    useEffect(() => { loadData() }, [loadData])
    return (
        <Paper className={styles.wrapper}>
            <div className={styles.form}>
                <TextField className={styles.formInput} label="email" value={email} onChange={(event) => setEmail(event.currentTarget.value)} ></TextField>
                <MuiPickersUtilsProvider utils={DateFnsUtils}>
                    <KeyboardDatePicker
                        className={styles.formInput}
                        disableToolbar
                        variant="inline"
                        format="dd/MM/yyyy"
                        label="Min date"
                        value={minDate}
                        onChange={val => setMinDate(val)}
                    />
                    <KeyboardDatePicker
                        className={styles.formInput}
                        disableToolbar
                        variant="inline"
                        format="dd/MM/yyyy"
                        label="Max date"
                        value={maxDate}
                        onChange={val => setMaxDate(val)}
                    />
                </MuiPickersUtilsProvider>
            </div>
            <div className={styles.chart}>
                {/* <ResponsiveCalendar
                    data={data}
                    from="minDate"
                    to="maxDate"
                    emptyColor="#eeeeee"
                    colors={['#61cdbb', '#97e3d5', '#e8c1a0', '#f47560']}
                    margin={{ top: 40, right: 40, bottom: 40, left: 40 }}
                    yearSpacing={40}
                    monthBorderColor="#ffffff"
                    dayBorderWidth={2}
                    dayBorderColor="#ffffff"
                    legends={[
                        {
                            anchor: 'bottom-right',
                            direction: 'row',
                            translateY: 36,
                            itemCount: 4,
                            itemWidth: 42,
                            itemHeight: 36,
                            itemsSpacing: 14,
                            itemDirection: 'right-to-left'
                        }
                    ]}
                /> */}
                <ResponsiveLine
                    data={data}
                    margin={{ top: 50, right: 110, bottom: 50, left: 60 }}
                    xScale={{ type: 'point' }}
                    yScale={{ type: 'linear', min: 'auto', max: 'auto', stacked: true, reverse: false }}
                    yFormat=" >-.2f"
                    axisTop={null}
                    axisRight={null}
                    axisBottom={{
                        orient: 'bottom',
                        tickSize: 5,
                        tickPadding: 5,
                        tickRotation: 0,
                        legend: 'Applications',
                        legendOffset: 36,
                        legendPosition: 'middle'
                    }}
                    axisLeft={{
                        orient: 'left',
                        tickSize: 5,
                        tickPadding: 5,
                        tickRotation: 0,
                        legend: 'timeused',
                        legendOffset: -40,
                        legendPosition: 'middle'
                    }}
                    pointSize={10}
                    pointColor={{ theme: 'background' }}
                    pointBorderWidth={2}
                    pointBorderColor={{ from: 'serieColor' }}
                    pointLabelYOffset={-12}
                    useMesh={true}
                    legends={[
                        {
                            anchor: 'bottom-right',
                            direction: 'column',
                            justify: false,
                            translateX: 100,
                            translateY: 0,
                            itemsSpacing: 0,
                            itemDirection: 'left-to-right',
                            itemWidth: 80,
                            itemHeight: 20,
                            itemOpacity: 0.75,
                            symbolSize: 12,
                            symbolShape: 'circle',
                            symbolBorderColor: 'rgba(0, 0, 0, .5)',
                            effects: [
                                {
                                    on: 'hover',
                                    style: {
                                        itemBackground: 'rgba(0, 0, 0, .03)',
                                        itemOpacity: 1
                                    }
                                }
                            ]
                        }
                    ]}
                />
            </div>
        </Paper>
    )
};

export default ActivitiesChart;