import { getNavigations } from "@ijl/cli";

const navigations = getNavigations("innometrics_dashboard");

export const baseUrl = navigations["innometrics_dashboard"];

export const URLs = {
  root: {
    url: navigations["innometrics_dashboard.app"],
  },
  forms: {
    url: navigations["link.innometrics_dashboard.app.forms"],
  },
  login: {
    url: navigations["link.innometrics_dashboard.app.login"], 
  }
};
