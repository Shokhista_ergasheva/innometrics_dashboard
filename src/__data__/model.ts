export type Link = {
    href: string;
    label: string;
}

export enum Size {
    lg = 'lg',
    md = 'md',
    sm = 'sm',
    xs = 'xs',
}

export type BooString = boolean | string;
export interface Category{
    
        catdescription: string,
        catname: string,
        timeused: string
      
}