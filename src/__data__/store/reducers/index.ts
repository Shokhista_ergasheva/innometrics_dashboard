import { combineReducers } from 'redux';

import { reducer as registrationConfirmReducer, RegistrationConfirmState } from '../slices/registration/confirm-stage';
import { RecoverloginInputState, reducer as RecoverLoginInputReducer } from '../slices/recover-login/input-stage';

import registrationInputStageReducer, { RegistrationInputStageState } from './registration/input-stage';
import loginReducer, { } from './login';

export type AppStore = {
    registration: {
        inputStage: RegistrationInputStageState;
        confirm: RegistrationConfirmState;
    };
    recoverLogin: {
        inputStage: RecoverloginInputState;
    };
    login: any;
}


export default combineReducers<AppStore>({
    registration: combineReducers({
        inputStage: registrationInputStageReducer,
        confirm: registrationConfirmReducer,
    }),
    recoverLogin: combineReducers({
        inputStage: RecoverLoginInputReducer,
    }),
    login: loginReducer,
});
