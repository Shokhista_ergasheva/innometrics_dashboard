
const initialState = {
    loading: false,
    error: false,
    data: null
}

export default function reducer(state = initialState, action) {
    switch (action.type) {
        case 'fetch':

            return {
                loading: true,
                error: false,
                data: null  
            };
        case 'success':

            return {
                loading: false,
                error: false,
                data: action.payload
            };
        case 'error':

            return {
                loading: false,
                error: true,
                data: null
            };

        default:
            return state;
    }

}