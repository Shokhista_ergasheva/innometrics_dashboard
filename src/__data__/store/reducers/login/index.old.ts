import * as types from '../../../action-types';

type LoginForm = {
    password: string;
    login: string;
}

export type LoginState = {
    loading: boolean;
    data: { token: string };
    error: boolean | string;
    validationError: boolean | string;
    form: LoginForm;
}

const initialState: LoginState = {
    loading: false,
    data: null,
    error: false,
    validationError: false,
    form: {
        password: '',
        login: '',
    }
}

const handleSubmit = (state) => ({ ...state, loading: true, error: false });
const handleSuccess = (state, action) => ({ ...state, loading: false, data: action.payload });
const handleFailure = (state, action) => ({ ...state, loading: false, error: action.payload });
const handleSetValidationError = (state, action) => ({ ...state, validationError: action.payload });
const handleSetFormLogin = (state, action) => ({
    ...state,
    error: false,
    validationError: false,
    form: {
        ...state.form,
        login: action.payload
    }
});
const handleSetFormPassword = (state, action) => ({
    ...state,
    error: false,
    validationError: false,
    form: {
        ...state.form,
        password: action.payload
    }
});

const handlers = {
    [types.LOGIN.SUBMIT]: handleSubmit,
    [types.LOGIN.SUCCESS]: handleSuccess,
    [types.LOGIN.FAILURE]: handleFailure,
    [types.LOGIN.SET_VALIDATION_ERROR]: handleSetValidationError,
    [types.LOGIN.FORM_LOGIN_CHANGE]: handleSetFormLogin,
    [types.LOGIN.FORM_PASSWORD_CHANGE]: handleSetFormPassword
}

export default function (state = initialState, action) {
    return handlers[action.type] ? handlers[action.type](state, action) : state;
}
