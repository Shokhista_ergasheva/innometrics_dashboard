import { createSlice } from '@reduxjs/toolkit';

import { BooString } from '../../../model';

export type RecoverloginInputState = {
    error: BooString;
    email: string;
    loading: boolean;
    data: unknown;
}

const initialState: RecoverloginInputState = {
    error: false,
    email: '',
    loading: false,
    data: null,
}

const slice = createSlice({
    name: 'recover-login-input',
    initialState,
    reducers: {
        changeEmail(state, action) {
            state.email = action.payload;
            state.error = false;
        },
        submit(state) {
            state.loading = true;
            state.error = false;
        },
        success(state, action) {
            state.loading = false;
            state.data = action.payload;
        },
        failure(state, action) {
            state.loading = false;
            state.error = action.payload;
        },
        reset(state) {
            state.data = null;
            state.email = '';
            state.error = false;
            state.loading = false;
        }
    }
})

export const { changeEmail, submit, success, failure, reset } = slice.actions;
export const reducer = slice.reducer;
