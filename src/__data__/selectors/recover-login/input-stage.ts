import { createSelector } from 'reselect';

import { AppStore } from '../../store/reducers';
import { RecoverloginInputState } from '../../store/slices/recover-login/input-stage';

const rootSelector = createSelector<AppStore, AppStore, RecoverloginInputState>(state => state, state => state.recoverLogin.inputStage);

export const emailSelector = createSelector(rootSelector, state => state.email);
export const dataSelector = createSelector(rootSelector, state => state.data);
export const errorSelector = createSelector(rootSelector, state => state.error);
export const loadingSelector = createSelector(rootSelector, state => state.loading);
