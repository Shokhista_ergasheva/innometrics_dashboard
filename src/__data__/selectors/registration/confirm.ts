import { createSelector } from 'reselect';

import { AppStore } from '../../store/reducers';
import { RegistrationConfirmState } from '../../store/slices/registration/confirm-stage';

const rootSelector = createSelector<AppStore, AppStore, RegistrationConfirmState>(state => state, state => state.registration.confirm);

export const dataSelector = createSelector(rootSelector, state => state.data);
export const codeSelector = createSelector(rootSelector, state => state.code);
export const errorSelector = createSelector(rootSelector, state => state.error);
export const loadingSelector = createSelector(rootSelector, state => state.loading);
