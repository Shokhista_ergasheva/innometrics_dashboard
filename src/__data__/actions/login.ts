// import { Callbacks } from "cypress/types/jquery";
import { authBhAxios } from "../../utils";


function submitLogin(email, password){
    return function (dispatch){
        dispatch({type:'fetch'})
        authBhAxios({
            method: 'POST',
            data: {
                "email": email,
                "password": password
              },
            url: '/login',
        })
        .then((responce)=>{
            dispatch({type:'success', payload: {  ...responce.data, email: email } })
        })
        .catch((error)=>{
            console.log(error.message)
            dispatch({type:'error'})
        })
    }
}

export {submitLogin};