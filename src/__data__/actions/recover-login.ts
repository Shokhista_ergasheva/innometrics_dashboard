import { getConfigValue } from '@ijl/cli';
import { submit, success, failure, reset } from '../store/slices/recover-login/input-stage';

export default (email) => async (dispatch) => {
    dispatch(submit());
    const baseApiUrl = getConfigValue('innometrics_dashboard.api');
    const response = await fetch(baseApiUrl + '/recover/login', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json;charset=utf-8',
        },
        body: JSON.stringify({ email })
    });

    if (response.ok) {
        try {
            const result = await response.json();
            dispatch(success(result));
        } catch (error) {
            dispatch(failure('Неизвестная ошибка'));
        }
    } else {
        try {
            const result = await response.json();
            dispatch(failure(result.error));
        } catch (error) {
            dispatch(failure('Неизвестная ошибка'));
        }
    }
}
/**
 * Сбрасываем состояние редьюсера Восстановление пароля.
 */
export const resetRecoverLogin = () => dispatch => {
    dispatch(reset());
}
