const router = require("express").Router();
const forge = require("node-forge")
const _ = require("lodash")
const qs = require("qs")

const TWO_SECONDS = 2000;

const axios = require('axios');
const axiosApi = axios.create({
    baseURL: 'https://innometric.guru:9091',
    headers: {
        'Content-Type': 'application/json;charset=utf-8',
    },
});

const wait = (time = TWO_SECONDS) => (req, res, next) =>
    setTimeout(next, time);

router.post('/login', (req, res)=>{
    setTimeout(() => {
        if(req.body.email==='error'){
            
            res.status(401).send(require('./mocks/login/error'))
        }else{
            res.status(200).send(require('./mocks/login/success'))
        }
    }, 1000);
});

router.get('/V1/cumulativeReport', (req, res) => {
    axiosApi(`/Reports/cumulativeReport?${qs.stringify(req.query)}`).then(value => res.json(value.data)).catch((value) => {
        console.error(value);
        res.json({ error: value.message });
    })
});

router.get('/V1/Reports/cumulativeReport', (req, res) => {
    res.send(require('./mocks/cumulativeReports/success'))
} );

router.get('/V1/categoryTimeReport', (req, res) => {
    axiosApi(`/Reports/categorytimeReport?${qs.stringify(req.query)}`).then(value => res.json(value.data)).catch((value) => {
        console.error(value);
        res.json({ error: value.message });
    })
});

router.post('/forms', (req, res) => {
    if (req.body.login === 'error') {
        return res.status(400).send({ message: 'Закрыто на обед!' });
    }
    // res.send(require('./mocks/login/success'));
    // res.status(500).send({"code":2,"error":"Не верный логин или пароль"});
});

router.post('/login', (req, res) => {
    if (req.body.login === 'error') {
        return res.status(400).send({ message: 'Закрыто на обед!' });
    }
    res.send(require('./mocks/login/success'));
    // res.status(500).send({"code":2,"error":"Не верный логин или пароль"});
});

router.post('/confirm/email', wait(), (req, res) => {
    res.send(require('./mocks/confirm/email/success'));
    // res.status(400).send(require('./mocks/confirm/email/error'));
});

router.post('/register', (req, res) => {
    setTimeout(() => {
        // res.send(require('./mocks/register/success'));
        res.status(400).send(require('./mocks/register/error'));
    }, TWO_SECONDS)
});

router.post('/recover/password/new', (req, res) => {
    setTimeout(() => res.send(require('./mocks/recover/password/new/success')), 5000);
    // res.status(400).send(require('./mocks/recover/password/new/error'));
});

router.post('/recover/password/confirm', (req, res) => {
    res.send(require('./mocks/recover/password/confirm/success'));
    // res.status(400).send(require('./mocks/recover/password/confirm/error'));
});

router.post('/recover/password', (req, res) => {
    res.send(require('./mocks/recover/password/success'));
    // res.send(require('./mocks/recover/password/error'));
});

router.post('/recover/login/new', (req, res) => {
    res.send(require('./mocks/recover/login/new/success'));
    // res.status(400).send(require('./mocks/recover/login/new/error'));
});

router.post('/recover/login/confirm', (req, res) => {
    console.log()
    const { pubkey } = req.body

    const parsedPubKey = forge.pki.publicKeyFromPem(pubkey)
    const login = 'DeadPool3'
    const buffer = forge.util.createBuffer(login, 'utf8').getBytes()
    const encryptedLogin = parsedPubKey.encrypt(buffer, 'RSA-OAEP')

    const encryptedLoginBase64 = forge.util.encode64(encryptedLogin)

    const ans = require('./mocks/recover/login/confirm/success')

    _.set(ans, 'login', encryptedLoginBase64)
    res.send(ans);
    // шифрование
    // res.status(400).send(require('./mocks/recover/login/confirm/error'));
});

router.post('/recover/login', (req, res) => {
    res.send(require('./mocks/recover/login/success'));
    // res.send(require('./mocks/recover/login/error'));
});

module.exports = router;
