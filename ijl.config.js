const pkg = require("./package");

module.exports = {
  apiPath: "stubs/api",
  webpackConfig: {
    output: {
      publicPath: `/static/innometrics_dashboard/${pkg.version}/`,
    },
    module: {
      rules: [
        {
          test: /\.css$/i,
          use: [
            { loader: "style-loader" },
            {
              loader: "css-loader",
              options: {
                modules: {
                  mode: "local",
                  exportGlobals: true,
                  // localIdentName:  isProd ?
                  //     '[hash:base64]'
                  //     :'[path]--[name]__[local]--[hash:base64:3]',
                  // localIdentContext: Path2D.resolve(_dirname, 'src'),
                  exportLocalsConvention: "camelCase",
                },
              },
            },
            {
              loader: "postcss-loader",
              options: {
                postcssOptions: {
                  plugins: [
                    [
                      "postcss-preset-env",
                      {
                        // Options
                      },
                    ],
                  ],
                },
              },
            },
          ],
        },
      ],
    },
  },
  navigations: {
    "innometrics_dashboard.app": "/innometrics_dashboard",
    "link.innometrics_dashboard.app.forms": "/innometrics_dashboard/forms",
    "link.innometrics_dashboard.app.login": "/innometrics_dashboard/login"
  },
  config: {
    'innometrics_dashboard.api': "/api"
  },
};
