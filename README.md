# Innometrics
Description: 
The system aims at collecting and evaluating the developers activities and processes. Following which tries to evaluate and predict the software development process efficiency. 

## Dashboard
Analytical dashboard - will give the user clear vision of the performance trends and potential problems.


### MVP1
1. Authorization, registration.
2. Dashboard

* Side bar nav (Home page with coarse granular data, Chart 1 detailed, Graph 2 detailed..)
* Top nav (Name, profile photo, location, logo, user details page)

### Installation
```shell
npm install
```
### Running
```shell
npm start
```

## Figma 
Here is the [template](https://www.figma.com/file/odvJjlFTMKslBM1tTusdoa/Dashboard-design?node-id=0%3A1) of the project.  

### Have feedback or questions?
Contact me via email: ergashevashohista@gmail.com